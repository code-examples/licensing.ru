---
layout: services
title: Licensing & Licenses
subTitle: Лицензирование и лицензии
permalink: /services/
img: notebook-win.png
category: Services
---

### Услуги:

<br/>

Есть или планируются задачи по автоматизации, разработке, внедрению программного обеспечения? Пишите на адреса из страницы контактов.

Умеем работать с widows, linux (centos, ubuntu), серверами баз данных (postgresql, mysql, oracle), сайтами и т.д.
