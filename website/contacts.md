---
layout: contacts
title: Licensing & Licenses
subTitle: Лицензирование и лицензии
permalink: /contacts/
img: ubuntu.png
category: Services
---

### Контакты:

<br/>
Россия, Москва.  
Телефон: 8 (915) 297-47-20  
Электронная почта: licensing.ru@mail.ru  
Твиттер: @licensing_ru
