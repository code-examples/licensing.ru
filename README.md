# [licensing.ru](https://licensing.ru) source codes

<br/>

### Run licensing.ru on localhost

    # vi /etc/systemd/system/licensing.ru.service

Insert code from licensing.ru.service

    # systemctl enable licensing.ru.service
    # systemctl start licensing.ru.service
    # systemctl status licensing.ru.service

http://localhost:4061
